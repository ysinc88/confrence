class LeadersController < ApplicationController
  before_action :set_leader, only: [:show, :edit, :update, :destroy, :test, :rsvp]
   http_basic_authenticate_with name: ENV['admin'], password:  ENV['pass'], only: [:index]
  skip_before_filter :verify_authenticity_token, :only => [:update]
  
   
  
def rsvp
	oui = params[:rsvp]
	@leader.update!(:rsvp => oui)
end

  
  def test
    del = RsvpMailer.rsvp_email(Leader.find_by_contact_email("admin@yairsegal.com")).deliver_now
    if del
      flash[:notice] = "Mail sent"
      redirect_to root_path
    else
      flash[:notice] = "Mail not sent"
      redirect_to root_path
    end
  end
  def index
  
    @leaders = Leader.all
    @sumGradStudent = Leader.where(:title_id => [3,4]).count
    @sumDocProf = Leader.where(:title_id => [1,2]).count
    @sumGuest= Leader.where(:title_id => 5).count
    
    @sumIdentity = Leader.where(:seat_id => 1).count
    @sumDistance = Leader.where(:seat_id => 2).count
    @sumCharisma = Leader.where(:seat_id => 3).count
    @sumRsvp = Leader.where(:rsvp => 1).count
    @sumTotal = Leader.all.count
    
    respond_to do |format|
      format.html
      format.csv { send_data @leaders.to_csv }
      format.xls # { send_data @leaders.to_csv(col_sep: "\t") }
    end
    
  end
  
  
  

  # GET /leaders/1
  # GET /leaders/1.json
  def show
  end

  # GET /leaders/new
  def new
    @leader = Leader.new
    
    #Notifier.welcome_email(@leader).deliver
  end
  
  def closed

  
  end

  # GET /leaders/1/edit
  def edit
  end

  # POST /leaders
  # POST /leaders.json
  def create
    @leader = Leader.new(leader_params)
    
    respond_to do |format|
      if @leader.save
      Notifier.welcome_email(@leader).deliver
        format.html { redirect_to new_leader_path, notice: "Thank you for registering" }
        format.json { render :show, status: :created, location: @leader }
      else
        format.html { render :new }
        format.json { render json: @leader.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leaders/1
  # PATCH/PUT /leaders/1.json
  def update
    respond_to do |format|
      if @leader.update(leader_params)
	
        format.html { redirect_to @leader, head: :no_content, notice: 'Leader was successfully updated.' }
        #format.json { render :show, status: :ok, location: @leader }
        format.json { respond_with_bip(@leader) }
	format.json { render :show, status: :ok, location: @leader }      
else
        format.html { render :edit }
        format.json { render json: @leader.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leaders/1
  # DELETE /leaders/1.json
  def destroy
    @leader.destroy
    respond_to do |format|
      format.html { redirect_to leaders_url, notice: 'Leader was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def thank_you
    
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leader
      @leader = Leader.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leader_params
      params.require(:leader).permit(:seat_id, :title_id, :first_name, :last_name, :contact_email, :phone, :locale, :rsvp)
    end
end
