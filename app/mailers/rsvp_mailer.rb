class RsvpMailer < ApplicationMailer
	
  default from: "registration@leadersil.org"
  
require 'icalendar'

# Create a calendar with an event (standard method)

    
	def rsvp_email(leader)
		@leader = leader
    
    cal = Icalendar::Calendar.new
    cal.event do |e|
      e.dtstart     = DateTime.civil(2015, 11, 23, 12, 00)
      e.dtend       = DateTime.civil(2015, 11, 23, 19, 30)
      e.summary     = "כנס מנהיגות עכשיו"
      e.description = "כנס על מנהיגות ומונהגים בסמוך ליום השנה הראשון לפטירתו של פרופ' בעז שמיר"
      e.ip_class    = "PRIVATE"
    end

    cal.publish
    
    attachments['event.ics'] = { mime_type: 'application/ics', content: cal.to_ical }
    #attachments['brochure.pdf'] = File.read('brchure.pdf')
    #attachments.inline['brochure.pdf'] = File.read('public/brochure.pdf')
    
		mail(
			#to: @leader.email, 
			to: @leader.contact_email, 
			subject: 'אישור השתתפות - כנס מנהיגות עכשיו'
      
			)
  	end
end
