class Leader < ActiveRecord::Base

belongs_to :title
belongs_to :seat

has_secure_token :rsvp_token

extend FriendlyId
  friendly_id :rsvp_token, use: :slugged
  
validates :first_name, :last_name, :seat_id, presence: true
#validates_uniqueness_of :email
  
  def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |leader|
      csv << leader.attributes.values_at(*column_names)
    end
  end
end

  def self.gradstud
      if Leader.where(:title_id => [3,4]).count > 1
        return false
      end
  end

    def self.docprof
      if Leader.where(:title_id => [1,2]).count > 1
        return true
      end
  end
  
    def self.guest
      if Leader.where(:title_id =>5).count > 1
        return true
      end
  end


end