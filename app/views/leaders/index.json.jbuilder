json.array!(@leaders) do |leader|
  json.extract! leader, :id, :seat_id, :title_id, :first_name, :last_name, :email, :phone
  json.url leader_url(leader, format: :json)
end
