class CreateLeaders < ActiveRecord::Migration
  def change
    create_table :leaders do |t|
      t.string :seat_id
      t.string :title_id
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
    add_index :leaders, :seat_id
    add_index :leaders, :title_id
  end
end
