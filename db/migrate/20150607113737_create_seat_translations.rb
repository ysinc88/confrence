class CreateSeatTranslations < ActiveRecord::Migration

  def self.up
    Seat.create_translation_table!({
      :name => :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    Seat.drop_translation_table! :migrate_data => true
  end

end
