class AddRsvpColumsToLeaders < ActiveRecord::Migration
  def change
    add_column :leaders, :rsvp, :boolean, :default => false
    add_column :leaders, :rsvp_token, :string
  end
end
