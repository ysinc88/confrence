class AddSlugToLeaders < ActiveRecord::Migration
  def change
    add_column :leaders, :slug, :string
    add_index :leaders, :slug, unique: true
  end
end
