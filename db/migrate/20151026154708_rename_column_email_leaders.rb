class RenameColumnEmailLeaders < ActiveRecord::Migration
  def change
	rename_column :leaders, :email, :contact_email
  end
end
