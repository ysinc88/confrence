# Preview all emails at http://localhost:3000/rails/mailers/rsvp_mailer

class RsvpMailerPreview < ActionMailer::Preview
	
  def rsvp_mail_preview
    	RsvpMailer.rsvp_email(Leader.find_by_contact_email("onelove@ledyair.com"))
  end
  
end
